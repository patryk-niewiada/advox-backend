<?php
/**
 * Created by PhpStorm.
 * User: Patryk
 * Date: 10.04.2018
 * Time: 20:48
 */

namespace Advox;

/**
 * Napisz w języku PHP (7.*) klasę, które będzie przyjmowała podawane kolejno liczby naturalne, a na żądanie
 * użytkownika będzie pozwalała na uzyskanie informacji, czy większa była trzykrotność liczb parzystych, czy
 * dwukrotność nieparzystych.
 *
 * Class Zadanie1
 * @package Programa
 */
class Zadanie1
{

    /**
     * @var array
     */
    private $oddNumbers;

    /**
     * @var array
     */
    private $evenNumbers;

    /**
     * Zadanie1 constructor.
     * @param int[] ...$numbers
     */
    public function __construct(int ...$numbers)
    {
        array_walk($numbers, function($number) {
            if(0 === ($number % 2)) {
                $this->evenNumbers[] = $number;
                return;
            }
            $this->oddNumbers[] = $number;
        });
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getResult();
    }

    /**
     * @return int
     */
    private function getOddResult():int
    {
        if(empty($this->oddNumbers)) {
            return 0;
        }

        return array_sum($this->oddNumbers) * 2;
    }

    /**
     * @return int
     */
    private function getEvenResult():int
    {
        if(empty($this->evenNumbers)) {
            return 0;
        }

        return array_sum($this->evenNumbers) * 3;
    }

    /**
     * @return string
     */
    public function getResult():string
    {
        $oddResult = $this->getOddResult();
        $evenResult = $this->getEvenResult();

        if(0 === $oddResult && 0 === $evenResult) {
            return "Nie podano żadnych liczb wejściowych";
        }

        if($oddResult > $evenResult) {
            return "Dwukrotność liczb nieparzystych jest większa i wynosi {$oddResult}";
        }

        return "Trzykrotność liczb parzystych jest większa bądź równa i wynosi {$evenResult}";
    }

}

echo new Zadanie1(1,5,8);