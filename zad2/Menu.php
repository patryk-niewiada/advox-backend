<?php
/**
 * Created by PhpStorm.
 * User: Patryk
 * Date: 11.04.2018
 * Time: 22:52
 */

namespace Advox;


class Menu
{
    const DB_USER = "test";
    const DB_PASSWORD = "";
    const DB_HOST = "localhost";
    const DB_NAME = "parcial";

    /**
     * @var PDO
     */
    private $sql;

    /**
     * @var array
     */
    private $data;

    /**
     * Menu constructor.
     */
    public function __construct()
    {
        $this->sql = new \PDO($this->getConnectionString(), self::DB_USER, self::DB_PASSWORD);
        $this->data = $this->getData();
    }

    /**
     * @return string
     */
    private function getConnectionString():string
    {
        return "mysql:dbname=" . self::DB_USER . ";charset=utf8;host=" . self::DB_HOST;
    }
    /**
     * @return array
     */
    private function getData():array
    {
        $query = $this->sql->query('SELECT * FROM `menu`');
        return $query->fetchAll();
    }

    /**
     * @param int $parentId
     * @return array
     */
    public function buildTree(int $parentId):array
    {
        $result = [];

        foreach($this->getData() as $item) {
            if ((int) $item['parent_id'] === (int) $parentId) {
                $newItem = $item;
                $childrens = $this->buildTree($newItem['id']);

                if( ! empty($childrens)) {
                    $newItem['childrens'] = $childrens;
                }

                $result[] = $newItem;
            }
        }

        if (empty($result) > 0) {
            return [];
        }

        return $result;
    }

    public function buildHtmlMenu(array $tree):string
    {
        $html = "<ul>";
        foreach ($tree as $item) {
            $html .=  "<li>";
            $html .= "<a href=\"{$item['url']}\">{$item['anchor']}</a>";
            if (!empty($item['childrens'])) {
                $html .= $this->buildHtmlMenu($item['childrens']);
            }
            $html .= "</li>";
        }

        $html .= "</ul>";

        return $html;
    }

}