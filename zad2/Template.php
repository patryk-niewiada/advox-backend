<?php
/**
 * Created by PhpStorm.
 * User: Patryk
 * Date: 12.04.2018
 * Time: 00:37
 */

namespace Advox;


class Template
{
    /**
     * @param string|null $view
     * @param array $variables
     */
    public function __construct(string $view = null, array $variables = array()) {
        $viewPath = __DIR__ . "/" . $view . ".php";

        if(is_null($view) || !file_exists($viewPath) || !is_array($variables)) {
            return false;
        }

        if(is_array($variables) && !empty($variables)) {
            foreach($variables as $varName => $varValue) {
                ${$varName} = $varValue;
            }
        }

        ob_start();
        include $viewPath;
        ob_get_contents();
    }
}