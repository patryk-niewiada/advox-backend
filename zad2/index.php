<?php
require "Template.php";
require "Menu.php";

/**
 * Created by PhpStorm.
 * User: Patryk
 * Date: 12.04.2018
 * Time: 00:34
 */

$menu = new \Advox\Menu();

$tree = $menu->buildTree(0);

new \Advox\Template('layout', [
    'menu' => $menu->buildHtmlMenu($tree)
]);