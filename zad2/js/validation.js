var Validation = {
    valid: true,
    form: {},
    textFields: {},
    numberFields: {},
    selectFields: {},
    checkboxFields: {},
    init: function () {
        Validation.form = $('.js-form');
        Validation.textFields = Validation.form.find('[type=text].js-validate');
        Validation.numberFields = Validation.form.find('[type=number].js-validate');
        Validation.selectFields = Validation.form.find('select.js-validate');
        Validation.checkboxFields = Validation.form.find('[type=checkbox].js-validate');

        Validation.events();
    },
    events: function() {
        Validation.textFields.on('change keyup', Validation.textInputValidator);
        Validation.numberFields.on('change', Validation.numberInputValidator);
        Validation.selectFields.on('change', Validation.selectValidator);
        Validation.checkboxFields.on('change', Validation.checkboxValidator);

        Validation.form.submit(function (e) {
            Validation.valid = true;
            Validation.textFields.each(Validation.textInputValidator);
            Validation.numberFields.each(Validation.numberInputValidator);
            Validation.selectFields.each(Validation.selectValidator);
            Validation.checkboxFields.each(Validation.checkboxValidator);

            if( ! Validation.valid) {
                e.preventDefault();
                return false;
            }
        });
    },
    setErrorMessage: function(input) {
        var errorMessage = input.data('error-message');
        input.parents('.js-form__col').first().addClass('form__col--error').find('.js-form__error').html(errorMessage);
        Validation.valid = false;
    },
    removeErrorMessage: function(input) {
        input.parents('.js-form__col').first().removeClass('form__col--error').find('.js-form__error').html('');
    },
    textInputValidator: function () {
        var input = $(this);
        var minLength = input.attr('minlength');
        var maxLength = input.attr('maxlength');
        var value = input.val();

        if(value.length > maxLength || value.length < minLength) {
            Validation.setErrorMessage(input);
            return true;
        }

        Validation.removeErrorMessage(input);
    },
    numberInputValidator: function() {
        var input = $(this);
        // W przypadku zmiany przez inspektor odczytuje nową wartość
        var minValue = /*parseInt(input.attr('min'))*/18;
        var maxValue = /*parseInt(input.attr('max'))*/99;
        var value = parseInt(input.val());

        if(value < minValue || value > maxValue || isNaN(value)) {
            Validation.setErrorMessage(input);
            return;
        }

        Validation.removeErrorMessage(input);

    },
    checkboxValidator: function() {
        var checkbox = $(this);

        if( ! checkbox.is(':checked')) {
            Validation.setErrorMessage(checkbox);
            return;
        }

        Validation.removeErrorMessage(checkbox);
    },
    selectValidator: function() {
        var select = $(this);
        var selectedOptionValue = select.find(':selected').val();

        if("undefined" === typeof selectedOptionValue || "" === selectedOptionValue) {
            Validation.setErrorMessage(select);
            return;
        }

        Validation.removeErrorMessage(select);
    }
};

$(document).ready(Validation.init);