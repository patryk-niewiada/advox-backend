<!doctype html>
<html lang="pl-PL">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Programa - Zadanie 2</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/main.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <header>
            <h1>
                Patryk Niewiada
            </h1>
        </header>

        <section>
            <article>
                <header>
                    <h2>Dane osobowe</h2>
                </header>
                <div class="article__row">
                    <div class="article__col">
                        <b>Data urodzenia:</b>
                    </div>
                    <div class="article__col">
                        12.01.1993 r.
                    </div>
                </div>
                <div class="article__row">
                    <div class="article__col">
                        <b>Adres zamieszkania:</b>
                    </div>
                    <div class="article__col">
                        ul. Powstańców Wlkp. 4/2 61-895 Poznań
                    </div>
                </div>
                <div class="article__row">
                    <div class="article__col">
                        <b>Numer kontaktowy:</b>
                    </div>
                    <div class="article__col">
                        <a href="tel:602646602">602 646 602</a>
                    </div>
                </div>
                <div class="article__row">
                    <div class="article__col">
                        <b>Adres e-mail:</b>
                    </div>
                    <div class="article__col">
                        <a href="mailto:patryk.niewiada@gmail.com">patryk.niewiada@gmail.com</a>
                    </div>
                </div>
            </article>
            <article>
                <header>
                    <h2>Wykształcenie</h2>
                </header>
                <div class="article__row">
                    <div class="article__col">
                        <b>2014 - 2016</b>
                    </div>
                    <div class="article__col">
                        <b>Wyższa Szkoła Bankowa w Poznaniu</b>
                        <i>Informatyka, studia niestacjonarne</i>
                    </div>
                </div>
            </article>
            <article>
                <header>
                    <h2>Doświadczenie</h2>
                </header>
                <div class="article__row">
                    <div class="article__col">
                        <b>06.2015 - dziś</b>
                    </div>
                    <div class="article__col">
                        <b>GoldenSubmarine</b>
                        <i>Full-Stack Web developer</i>
                        <ul>
                            <li>
                                Tworzenie aplikacji internetowych w&nbsp;oparciu o&nbsp;gotowe specyfikacje techniczne z&nbsp;wykorzystaniem technologii (PHP, HTML,CSS,JS)
                            </li>
                            <li>
                                Rozwój oraz wspieranie istniejących aplikacji
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="article__row">
                    <div class="article__col">
                        <b>04.2014 - 05.2015</b>
                    </div>
                    <div class="article__col">
                        <b>Artflash Interactive</b>
                        <i>Full-Stack Web developer</i>
                        <ul>
                            <li>
                                Rozwój oraz wspieranie aplikacji internetowych napisanych w&nbsp;PHP
                            </li>
                            <li>
                                Utworzenie natywnej aplikacji mobilnej w&nbsp;oparciu o&nbsp;PhoneGap
                            </li>
                            <li>
                                Pomoc przy rozwoju wewnętrznego systemu zarządzania czasem pracy
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="article__row">
                    <div class="article__col">
                        <b>06.2013 - 03.2014</b>
                    </div>
                    <div class="article__col">
                        <b>Agencja Interaktywna Aweo</b>
                        <i>Web developer</i>
                        <ul>
                            <li>
                                Rozwój wewnętrznego systemu do pozycjonowania
                            </li>
                            <li>
                                Uczestnictwo w&nbsp;projekcie aplikacji wykrywającej plagiat w&nbsp;sieci
                            </li>
                        </ul>
                    </div>
                </div>
            </article>
            <article>
                <header>
                    <h2>Języki</h2>
                </header>
                <div class="article__row">
                    <div class="article__col">
                        <b>j.&nbsp;angielski</b>
                    </div>
                    <div class="article__col">
                        <p>
                            Znajomość na poziomie średnio zaawansowanym w&nbsp;mowie i&nbsp;piśmie
                        </p>
                    </div>
                </div>
            </article>
            <article>
                <header>
                    <h2>Umiejętności</h2>
                </header>
                <div class="article__row">
                    <div class="article__col">
                        <b>06.2013 - 03.2014</b>
                    </div>
                    <div class="article__col">
                        <ul>
                            <li>
                                HTML/HTML5, CSS/CSS3, JavaScript, jQuery, PHP, MySql, GIT, SVN
                            </li>
                            <li>
                                Podstawowa znajomość obsługi Adobe Photoshop
                            </li>
                            <li>
                                Prawo jazdy, kat. B
                            </li>
                        </ul>
                    </div>
                </div>
            </article>
            <article>
                <i>
                    Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w&nbsp;przesłanym CV dla potrzeb niezbędnych w&nbsp;procesie rekrutacji, zgodnie z&nbsp;ustawą z&nbsp;dnia 29.08.1997 roku o&nbsp;Ochronie danych Osobowych (Dz.U.Nr. 133 poz.883)
                </i>
            </article>
            <div class="form">
                <form action="#" method="POST" novalidate class="js-form">
                    <div class="form__row">
                        <div class="js-form__col form__col">
                            <label for="first_name">Imię: <abbr title="Pole wymagane">*</abbr></label>
                            <input class="js-validate" id="first_name" placeholder="Podaj swoje imię" data-error-message="Pole musi zawierać od 3 do 50 znaków" type="text" minlength="3" maxlength="50" name="first_name">
                            <div class="js-form__error form__error"></div>
                        </div>
                        <div class="js-form__col form__col">
                            <label for="last_name">Nazwisko: <abbr title="Pole wymagane">*</abbr></label>
                            <input class="js-validate" id="last_name" placeholder="Podaj swoje nazwisko" data-error-message="Pole musi zawierać od 3 do 50 znaków" type="text" minlength="3" maxlength="50" name="last_name">
                            <div class="js-form__error form__error"></div>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="js-form__col form__col">
                            <label for="age">Wiek: <abbr title="Pole wymagane">*</abbr></label>
                            <input class="js-validate" id="age" placeholder="Podaj swój wiek" data-error-message="Podaj wiek - przedział od 18 do 99" type="number" min="18" max="99" name="age">
                            <div class="js-form__error form__error"></div>
                        </div>
                        <div class="js-form__col form__col">
                            <label for="sex">Płeć: <abbr title="Pole wymagane">*</abbr></label>
                            <select class="js-validate" id="sex" name="sex" data-error-message="Musisz wybrać płeć">
                                <option value="" >Wybierz płeć</option>
                                <option value="female">Kobieta</option>
                                <option value="male">Miężczyzna</option>
                            </select>
                            <div class="js-form__error form__error"></div>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="js-form__col form__col form__col--big">
                            <label for="agreement">
                                <input class="js-validate" type="checkbox" value="1" id="agreement" data-error-message="Musisz wyrazić zgodę">
                                Wyrażenie zgody na przetwarzanie danych osobowych. <abbr title="Pole wymagane">*</abbr>
                            </label>
                            <div class="js-form__error form__error"></div>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__col form__col--big">
                            <input type="submit" value="Wyślij">
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <aside>
            <nav>
                <div id="menu">
                    <?php echo $menu ?: ''; ?>
                </div>
            </nav>
        </aside>
        <footer></footer>
    </body>
</html>
