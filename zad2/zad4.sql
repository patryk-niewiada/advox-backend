CREATE TABLE `menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `parent_id` INT NULL DEFAULT 0,
  `url` VARCHAR(255) NOT NULL,
  `anchor` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

INSERT INTO `menu` VALUES
(1, 0, '#', 'Kategoria główna'),
(2, 1, '#', 'Podkategoria 1'),
(3, 1, '#', 'Podkategoria 2'),
(4, 2, '#', 'Produkt A'),
(5, 2, '#', 'Produkt B'),
(6, 3, '#', 'Produkt C'),
(7, 3, '#', 'Produkt D');
