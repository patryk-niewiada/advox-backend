<?php

/**
 * CMD:
 * php AddProduct.php <nazwa_produktu> <cena_produktu> <opis_produktu> <dostępność>
 * Przykład:
 * php AddProduct.php "Sanki" 159.80 "Sanki sportowe" 0
 */

require_once "bootstrap.php";

if(empty($argv[1])) {
    echo "Musisz podać nazwę produktu\n";
    exit;
}
if(empty($argv[2])) {
    echo "Musisz podać cenę produktu\n";
    exit;
}

$productName = $argv[1];
$productPrice = isset($argv[2]) ? $argv[2] : 0.00;
$productDescription = isset($argv[3]) ? $argv[3] : null;
$productAvailability = isset($argv[4]) ? $argv[4] : 0;

$product = new ProductEntity();
$product->setName($productName);
$product->setPrice($productPrice);
$product->setDescription($productDescription);
$product->setAvailability($productAvailability);

$entityManager->persist($product);
$entityManager->flush();

$productAvailabilityText = (1 === (int) $productAvailability) ? "Tak" : "Nie";
echo "ID {$product->getId()} :: Stworzono produkt: {$productName}, cena: {$productPrice}, opis: {$productDescription}, dostępny: {$productAvailabilityText}\n";