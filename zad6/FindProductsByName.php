<?php

/**
 * CMD:
 * php FindProductsByName.php <fraza>
 */

require_once "bootstrap.php";

if(empty($argv[1])) {
    echo "Musisz podać nazwę produktu\n";
    exit;
}

$phrase = $argv[1];

$products = $entityManager->getRepository('ProductEntity')->createQueryBuilder('product')
    ->where('product.name LIKE :productName')
    ->setParameter('productName', "%{$phrase}%")
    ->getQuery()
    ->getResult();

if(empty($products)) {
    echo "Nie znaleziono żadnego produktu";
    exit;
}

echo "Lista znalezionych produktów dla frazy {$phrase}:\n\n";

foreach ($products as $product) {
    echo "{$product->getName()}, cena: {$product->getPrice()}, opis: {$product->getDescription()}\n";
}