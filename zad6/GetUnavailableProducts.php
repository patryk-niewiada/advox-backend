<?php

/**
 * CMD:
 * php GetUnavailableProducts.php
 */
require_once "bootstrap.php";

$productRepository = $entityManager->getRepository('ProductEntity');
$products = $productRepository->findBy(['availability' => 0]);

if(empty($products)) {
    echo "Nie znaleziono żadnego produktu";
    exit;
}
echo "Lista niedostępnych produktów:\n\n";


foreach ($products as $product) {
    $productInfo = "{$product->getName()}, cena: {$product->getPrice()}, opis: {$product->getDescription()}";
    echo sprintf("-%s\n", $productInfo);
}