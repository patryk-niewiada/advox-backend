<?php
/**
 * CMD
 * sh vendor/bin/doctrine orm:schema-tool:update --force --dump-sql
 * Created by PhpStorm.
 * User: user
 * Date: 12.04.2018
 * Time: 09:14
 */

/**
 * Class ProductEntity
 * @Entity @Table(name="products")
 */
class ProductEntity
{
    /**
     * @var int
     * @Id @Column(type="integer") @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $name;

    /**
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @var float
     * @Column(type="decimal", precision=10, scale=2, nullable=true, options={"default" : 0})
     */
    protected $price;

    /**
     * @var boolean
     * @Column(type="boolean")
     */
    protected $availability;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription():string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice():float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function getAvailability():bool
    {
        return $this->availability;
    }

    /**
     * @param bool $availability
     */
    public function setAvailability(bool $availability)
    {
        $this->availability = $availability;
    }
}