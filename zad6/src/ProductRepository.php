<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.04.2018
 * Time: 13:44
 */

use Doctrine\ORM\EntityManager;

/**
 * Class ProductRepository
 */
class ProductRepository
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ProductRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $productName
     * @param float $productPrice
     * @param string $productDescription
     * @param bool $productAvailability
     * @return \ProductEntity
     */
    public function addProduct(string $productName, float $productPrice, string $productDescription = "", bool $productAvailability = false) : \ProductEntity
    {
        $product = new \ProductEntity();
        $product->setName($productName);
        $product->setPrice($productPrice);
        $product->setDescription($productDescription);
        $product->setAvailability($productAvailability);

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @return array
     */
    public function getUnavailableProducts():array
    {
        $productRepository = $this->entityManager->getRepository('ProductEntity');
        return $productRepository->findBy(['availability' => 0]);
    }

    /**
     * @param string $phrase
     * @return array
     */
    public function findProductsByName(string $phrase):array
    {
        return $this->entityManager->getRepository('ProductEntity')->createQueryBuilder('product')
        ->where('product.name LIKE :productName')
        ->setParameter('productName', "%{$phrase}%")
        ->getQuery()
        ->getResult();
    }
}